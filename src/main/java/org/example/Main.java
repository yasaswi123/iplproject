package org.example;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;
import java.io.*;
import java.lang.reflect.Array;
import java.util.*;
public class Main {
    public static List<String[]> readFile(String file){
        try(CSVReader reader=new CSVReader(new FileReader(file))){
            List<String[]>convertArray=reader.readAll();
            return convertArray;
        }
        catch(IOException|CsvException e){
            System.out.println(e.getMessage());
        }
        return null;
    }
    //1 Number of matches played per year of all the years in IPL.
    public static void problem1(List<String[]> matches){
        HashMap<String,Integer> totalMatches=new HashMap<String,Integer>();
        for(int index=1;index<matches.size();index++){
            if(totalMatches.containsKey(matches.get(index)[1])){
               int value=(totalMatches.get(matches.get(index)[1]));
               totalMatches.put(matches.get(index)[1],value+1);
            }
            else{
                totalMatches.put(matches.get(index)[1],1);
            }
        }
        System.out.println(totalMatches);
    }
    //2 Number of matches won of all teams over all the years of IPL
    public static void problem2(List<String[]> matches){
        HashMap<String,Integer> winners=new HashMap<>();
        for(int index=1;index<matches.size();index++){
            if(matches.get(index)[10]==null||matches.get(index)[10].isEmpty()){
                continue;
            }
            if(winners.containsKey(matches.get(index)[10])){
                int value=winners.get(matches.get(index)[10]);
                winners.put(matches.get(index)[10],value+1);
            }
            else{
                winners.put(matches.get(index)[10],1);
            }
        }
        System.out.println(winners);
    }
    //3 extra runs conceded per team in year 2016
    public static void problem3(List<String[]>matches,List<String[]>deliveries){
        ArrayList<String> identities=new ArrayList<>();
        for(int index1=1;index1<matches.size();index1++){
            if(matches.get(index1)[1].equals("2016")){
                identities.add(matches.get(index1)[0]);
            }
        }
        HashMap<String,Integer> extraRuns=new HashMap<>();
        for(int index2=1;index2<deliveries.size();index2++){
            if(identities.contains(deliveries.get(index2)[0])){
                if(extraRuns.containsKey(deliveries.get(index2)[3])){
                    int each_runs=Integer.parseInt(deliveries.get(index2)[16]);
                    int previous_runs=extraRuns.get(deliveries.get(index2)[3]);
                    extraRuns.put(deliveries.get(index2)[3],previous_runs+each_runs);
                }
                else{
                    int runs=Integer.parseInt(deliveries.get(index2)[16]);
                    extraRuns.put(deliveries.get(index2)[3],runs);
                }
            }

        }
        System.out.println(extraRuns);
    }
    //4 top 10 ecnomical bowlers in 2015
    public static  void problem4(List<String[]>matches,List<String[]>deliveries){
        ArrayList<String> identity=new ArrayList<>();
        for(int index1=1;index1<matches.size();index1++){
            if(matches.get(index1)[1].equals("2015")){
                identity.add(matches.get(index1)[0]);
            }
        }
        HashMap<String,ArrayList<Integer>> each_bowler=new HashMap<>();
        for(int index2=1;index2<deliveries.size();index2++){
            if(identity.contains(deliveries.get(index2)[0])){
                if(each_bowler.containsKey(deliveries.get(index2)[8])){
                    ArrayList<Integer> rating=each_bowler.get(deliveries.get(index2)[8]);
                    int runs=rating.get(1);
                    int balls=rating.get(0);
                    int totalRuns=Integer.parseInt(deliveries.get(index2)[17]);
                    rating.set(0,balls+1);
                    rating.set(1,runs+totalRuns);
                    each_bowler.put(deliveries.get(index2)[8],rating);
                }
                else{
                    ArrayList<Integer> rate=new ArrayList<>();
                    int totalRuns=Integer.parseInt(deliveries.get(index2)[17]);
                    rate.add(1);
                    rate.add(totalRuns);
                    each_bowler.put(deliveries.get(index2)[8],rate);
                }
            }
        }
        HashMap<String,Double> economical_rate=new HashMap<>();
        for(String bowler:each_bowler.keySet()){
            ArrayList<Integer> economic=each_bowler.get(bowler);
            double balls=(double) economic.get(0);
            double runs=(double) economic.get(1);
            double each_rating=(runs/balls)*6;
            economical_rate.put(bowler,each_rating);
        }
        ArrayList<Map.Entry<String,Double>> sorting=new ArrayList(economical_rate.entrySet());
        sorting.sort(Map.Entry.<String, Double>comparingByValue());
        Map<String,Double> sortingOrder=new LinkedHashMap<>();
        for(Map.Entry<String,Double>entry:sorting){
            sortingOrder.put(entry.getKey(),entry.getValue());
        }
        int count=0;
        for(String key:sortingOrder.keySet()){
            count++;
            if(count<=10){
                Double value=sortingOrder.get(key);
                System.out.println(key+" "+value);
            }
        }
    }
    //5 Highest number of players of matches in 2015
    public static  void problem5(List<String[]>matches){
        HashMap<String,Integer> playerofmatch=new HashMap<>();
        for(int index=1;index<matches.size();index++){
            if(matches.get(index)[1].equals("2015")){
                if(matches.get(index)[13].isEmpty()){
                   continue;
                }
                if(playerofmatch.containsKey(matches.get(index)[13])){
                    int win=playerofmatch.get(matches.get(index)[13]);
                    playerofmatch.put(matches.get(index)[13],win+1);
                }
                else{
                    playerofmatch.put(matches.get(index)[13],1);
                }
            }
        }
        ArrayList<Map.Entry<String,Integer>> sorting=new ArrayList<>(playerofmatch.entrySet());
        sorting.sort(Map.Entry.<String,Integer>comparingByValue());
        Map<String,Integer> sorted=new LinkedHashMap<>();
        for(Map.Entry<String,Integer>key:sorting){
            sorted.put(key.getKey(), key.getValue());
        }
        List<Map.Entry<String ,Integer>> convertList=new ArrayList<>(sorted.entrySet());
        Map.Entry<String,Integer> highestPlayerOfMatches=convertList.get(convertList.size()-1);
        System.out.println(highestPlayerOfMatches);
    }
    public static void main(String[] args) {
        String fileName1="/home/yasaswi/IdeaProjects/iplproject/src/main/java/org/example/matches.csv";
        String fileName2="/home/yasaswi/IdeaProjects/iplproject/src/main/java/org/example/deliveries.csv";
        List<String[]>matches=readFile(fileName1);
        List<String[]>deliveries=readFile(fileName2);
        problem1(matches);
        problem2(matches);
        problem3(matches,deliveries);
        problem4(matches,deliveries);
        problem5(matches);
    }
}